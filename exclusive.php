<?php
   include_once('header.php');
   ?>
<div class="container-full jackcontent">
   <div class="container">
      <div class="homecontenttop col-md-offset-3 col-md-9">
         <h1 class="homepara">The Career Engine Difference</h1>
         <h5 class="homepara1">A Short 1-2 lines of text to briefly introduce the video which can be played below:</h5>
         <div class="video">
            <img src="images/video.png" width="562" height="300">
         </div>
         <h4 class="homepara2">See how Career Engine can help you:</h4>
         <div class="boxes">
            <div class="box1">Become Job Ready</div>
            <div class="box2">Finding the Job</div>
            <div class="box3">Getting the Job</div>
            <div class="box4">Excelling at  the Job</div>
         </div>
      </div>
   </div>
</div>
<div class="container-full">
   <div class="container">
      <div class="col-md-12">
         <h1 class="partner">OUR PARTNERS</h1>
         <h3 class="partnerdesc">a short line to describe the nature of your partnerships</h3>
         <div class="circle">
            <ul>
               <li class="circle1"></li>
               <li class="circle2"></li>
               <li class="circle3"></li>
               <li class="circle4"></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="container-full middlecontent">
   <div class="container">
      <div class="middlecontenttop col-md-12">
         <div class="middle">
            <div class="col-md-3 downpara">
               <button class="tablinks active" onclick="openHead(event, 'downpara')"><img src=" images/thumbnail.png"></button>
            </div>
            <div class="col-md-3 downpara1">
               <button class="tablinks" onclick="openHead(event, 'downpara1')"><img src="images/thumbnail1.png"></button>
            </div>
            <div class="col-md-3 downpara2">
               <button class="tablinks" onclick="openHead(event, 'downpara2')"><img src="images/thumbnail2.png"></button>
            </div>
            <div class="col-md-3 downpara3">
               <button class="tablinks" onclick="openHead(event, 'downpara3')"><img src="images/thumbnail3.png"></button>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container-full downcontent">
   <div class="container">
      <div class="col-md-12">
         <div id="downpara">
            Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volutpat.
         </div>
      </div>
      <div class="col-md-12">
         <div id="downpara1">
            xxx
         </div>
      </div>
      <div class="col-md-12">
         <div id="downpara2">
            yyy
         </div>
      </div>
      <div class="col-md-12">
         <div id="downpara3">
            zzz
         </div>
      </div>
   </div>
</div>
<div class="container-full">
   <div class="container">
      <div class="col-md-12 bigvideo">
         <img src="images/bigvideo.png" />
      </div>
   </div>
</div>
<div class="container-full">
   <div class="container">
      <div class="col-md-4 module">
         <h3>Module 01:</h3>
         <h1>Communicate your way forward</h1>
         <h3>Highlighted module summary/ short description will appear here.</h3>
         <h6>Duration: 15:34 | Rating: <img src="images/star.png"></h6>
      </div>
      <div class="col-md-8 imgright">
         &nbsp;
      </div>
   </div>
</div>
</div>
<div class="container-full testi">
    <div class="container">
        <div class="test1"><h1>TESTIMONIALS</h1></div>
            <div class="middlecontenttop col-md-12">
                <div class="miniimages">
                    <div class="col-md-4 mini1">
                        <button class="tablinks active" onclick="openHead(event, 'minipara1')"><img src="images/minithumbnail1.png" class="img1"></button>
                    </div>

                    <div class="col-md-4 mini1">
                        <button class="tablinks active" onclick="openHead(event, 'minipara2')"><img src="images/minithumbnail.png" class="img2"></button>
                    </div>

                    <div class="col-md-4 mini1">
                        <button class="tablinks active" onclick="openHead(event, 'minipara3')"><img src="images/minithumbnail2.png" class="img3"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>   
        <div class="container">
            <div id="minipara1" class="row tabcontent" style="display: block;">
                <div class="col-md-12 mini">Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at. Donec pretium, dolor nec elementum aliquet, leo eros sodales nulla, ac cursus nunc eros id nibh
                </div>
                <div class="names"><h2>Juana dela Cruz</h2></div>
                <div class="names1"><h5>IT Specialist, 22</h5></div>
            </div>
            <div id="minipara2" class="row tabcontent">
                <div class="col-md-12 mini">Donec pretium, dolor nec elementum aliquet, leo eros sodales nulla, ac cursus nunc eros id nibh. Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at.
                </div>
                <div class="names"><h2>Juana dela Cruz</h2></div>
                <div class="names1"><h5>IT Specialist, 22</h5></div>
            </div>
            <div id="minipara3" class="row tabcontent">
                <div class="col-md-12 mini">Leo eros sodales nulla, ac cursus nunc eros id nibh, Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at. Donec pretium, dolor nec elementum aliquet.
                </div>
                <div class="names"><h2>Juana dela Cruz</h2></div>
                <div class="names1"><h5>IT Specialist, 22</h5></div>
            </div>
        </div>        
      
 

<div class="container-full bottomcontent">
    <div class="container">
        <div class="event">EVENTS</div>
            <div class="col-md-offset-4 col-md-8"></div>
            <div class="event1">Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them
        </div>
    </div>


    <div class="container">
        <div class="col-sm-4"> 
            <img src="images/bot1.png">
            </div>
                <div class= "col-md-4">
                    <img src="images/bot2.png">
                    </div>
                    <div class= "col-md-4">
                <img src="images/bot3.png">
            </div>
        
    </div>
</div>

<div class="date">Jul 21</div>
    <div class="eventtitle"><h1>Event Title</h1></div>
        <div class="eventpara">
        Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </div>
        <div class="read"><a href="#">read more...</a></div>
    </div>

<div class="date1">Jul</div>
    <div class="day">26</div>
    <div class="eventtitle1"><h1>Event Title</h1>
        </div>
        <div class="eventpara1">
        Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </div>
    <div class="read"><a href="#">read more...</a>
</div>
<div class="date2">Aug</div>
    <div class="day1">12</div>
    <div class="eventtitle2"><h1>Event Title</h1>
    </div>
        <div class="eventpara">
        <p>Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </p>
    </div>
    <div class="read2">read more...
</div> 
      </div>
    </div>
  </div>
</div>


<?php
   include_once('footer.php');
   ?>