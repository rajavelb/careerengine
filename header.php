<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Career Engine</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

     <!-- Custom CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="css/prasath.css" type="text/css" rel="stylesheet">
    <link href="css/parthi.css" type="text/css" rel="stylesheet">
    <link href="css/ganesh.css" type="text/css" rel="stylesheet">
    <link href="css/jack.css" type="text/css" rel="stylesheet">
    <link href="css/nisha.css" type="text/css" rel="stylesheet">
    <link href="css/kalai.css" type="text/css" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    
    <script src="js/myjs.js" type="text/javascript"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="container-full topheader">
    <div class="container" style="border:0px solid green;">
        Contact us: info@careerengine.org.ph  |  +63 912 345 6789
    </div>
</div>

<div class="container-full wrapperclass">
    <div class="col-md-offset-7 col-md-5 col-sm-offset-5 col-sm-7 col-xs-offset-1  col-xs-11 col-lg-offset-6 col-lg-6 wrapperinner">
        <div class="col-md-1 col-xs-1 wrappercutinnerfirst">
            &nbsp;
        </div>
        <div class="col-md-11 col-xs-11 wrappercutinner">
            <ul>
                <!-- <li class="cutimageclass"><img src="images/login.png"></li>
                <li><a href="#">Login</a></li> -->
                <li><a href="#" data-toggle="modal" data-target="#login-modal"><img src="images/login.png"></a></li>
                <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
                <li>|</li>
                <li><a  href="#" data-toggle="modal" data-target="#myModal">
                </a></li>
                <li>|</li>
                <li><a href="#">Language</a></li>
            </ul>
        </div>
    </div>
</div>



<!-- ganesh -->

<div class="container myheader">
    <div class="col-md-5 col-sm-12 col-xs-12 myheadersecond">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="images/logo.png">
        </div>
        <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-9 col-md-offset-3 col-md-6 logotext">
            Powered by DigiBayanihan
        </div>
    </div>
    <div class="col-md-offset-1 col-md-6">
        <nav class="navbar navbar-inverse mynav">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
              </button>
            </div>
            <div class="collapse navbar-collapse mylist" id="myNavbar">
              <ul class="nav navbar-nav">
                  <li class="activenav"><a href="#">HOME</a></li>
                  <li><a href="#">ABOUT US</a></li>
                  <li><a href="#">OUR COURSES</a></li>
                  <li><a href="#">BUSINESS</a></li>
                  <li><a href="#">EVENTS</a></li>
                  <li><a href="#"><img src="images/search.png"></a></li>
                  <li><a href="#"><img src="images/upload.png"></a></li>
                  <li><a href="#">SHARE</a></li>
                </ul>
            </div>
          
        </nav>
    </div>
    
</div>


<!-- Parthi Login -->

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Begin # DIV Form -->
         <div id="div-forms">
            <!-- Begin # Login Form -->
            <form id="login-form">
               <div class="modal-body">
                  <span class="input-adon" id="basic-addon1"><img src="images/formname.png"></span>
                  <input id="login_username" class="form-controll" type="text" placeholder="User Name" required>
                  <span class="input-adon" id="basic-addon1"><img src="images/signuplock.png"></span>
                  <input id="login_password" class="form-controll" type="password" placeholder="Password" required>
                  <div class="checkbox">
                     <label>
                     <input type="checkbox" class="Remember">&nbsp; Remember Me
                     </label>
                     <img src="images/questionmark.jpg" />
                     <button id="login_lost_btn" type="button" class="btn btn-link">Need Help</button>
                  </div>
               </div>
               <div class="modal-footer">
                  <div>
                     <button type="submit" class="btn-login btn-lg btn-block">LOG IN</button>
                  </div>
                  <div>
                     <button id="login_register_btn" type="button" class="btn btn-primary btn-lg btn-block">REGISTER</button>
                  </div>
               </div>
            </form>
            <!-- End # Login Form -->
            <!-- Begin | Register Form -->
            <form id="register-form" style="display:none;">
               <div class="modal-body">
                  <div id="div-register-msg">
                     <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                     <span id="text-register-msg">Register an account.</span>
                  </div>
                  <input id="register_username" class="form-controll" type="text" placeholder="Username" required>
                  <input id="register_email" class="form-controll" type="text" placeholder="E-Mail" required>
                  <input id="register_password" class="form-controll" type="password" placeholder="Password" required>
               </div>
               <div class="modal-footer">
                  <div>
                     <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                  </div>
                  <div>
                     <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                     <button id="register_lost_btn" type="button" class="btn btn-link">Need Help</button>
                  </div>
               </div>
            </form>
            <!-- End | Register Form -->
         </div>
         <!-- End # DIV Form -->
      </div>
   </div>
</div>

<!-- End Parthi login -->


<!-- Kalaiyarasan signup -->


<!-- Kalaiyarasan signup -->

<!-- Large modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style=" background-color:  #333366;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="text-center" id="myModalLabel"><a href="#" data-toggle="tab" style="color: white; font-style:italic; ">
                    SIGN UP</a></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 col-sm-6 col-md-6">
                    <form role="form">
                            <div class="form-group">
                            <img src="images\cancel.png" id="canimg">
                                <input type="text" name="firstname" id="kalai" class="form-control input-lg" placeholder="First Name" tabindex="1">
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="lastname" id="test" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <select  class="form-control" id="location" >

                                    <option>Location</option>
                                    <option>India</option>
                                    <option>Chennai</option>

                                </select>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <select  class="form-control"  id="education" >

                                    <option>Higher Educational Attainment</option>
                                    <option>UG</option>
                                    <option>PG</option>

                                </select>
                            </div>
                    </div>



                                    <option>Higher Educational Attainment</option>
                                    <option>UG</option>
                                    <option>PG</option>

                                </select>
                            </div>
                    </div>


                    

                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                            <img src="images\new-email-envelope-back-symbol-in-circular-outlined-button.png" id="emimg">
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="2">
                            </div>
                    </div>


                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <img src="images\lock-button.png" id="panimg">
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="1">
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <img src="images\lock-button.png" id="conimg">
                                <input type="password" name="confirmpassword" id="confirmpassword" class="form-control input-lg" placeholder="Confirm Password" tabindex="2">
                            </div>
                    </div>

                    
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="form-group">
                        <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Submit">
                        </div>
                    </div>



                    
                    <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:50px; font-size:85%" >
                                            

                                            <p id="pt">or sign up with one of these services</p> 

                                            </div>
                                        
                                        
                                    </div>
                                </div>     
                       
                           <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <img src="images/if_facebook_1807546.png" id="fbimg">
                            <input type="textarea" name="facebook" id="facebook" class="form-control input-lg" placeholder="Facebook" tabindex="1">
                            </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                            <img src="images/if_google-plus_1233007.png" id="gmimg">

                            <input type="textarea" name="google" id="gmail" class="form-control input-lg" placeholder="Google +" tabindex="2">
                            </div>
                            </div>
                                                                <!-- Nav tabs -->
                        
                            
                                
                                </form>
                            <!-- </div> -->
                        </div>
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>




<!-- end login -->
