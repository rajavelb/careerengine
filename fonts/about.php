<?php
include_once('header.php');
?>

<div class="container-full prasathcontent">
   <div class="col-md-6 wrapperleftdiv">
    <div class="col-md-12 leftdiv">
      <div class="col-md-8 col-md-offset-4">
        <div class="col-md-12">
          About Us
        </div>
        <div class="col-md-12">
            Did you know that the unemployment rate in the Philippines has raised to 6.6% in January 2017? The main reason behind this rise is the inability to understand the importance of career readiness. Being job ready is one of the biggest hurdles that stops you from joining a company that you’ve been dreaming to associate with. In the world of continuous changes in recruitment, technology, and professions, career readiness differentiates a person’s ability to have an edge over others to get the perfect job.
        </div>        
      </div>
    </div>
  </div>
  <div class="col-md-6 wrapperrightdiv">
    <div class="col-md-12 rightdiv">
      <div class="col-md-8">
        <div class="col-md-12">
            Why Us?
        </div>
        <div class="col-md-12">
          The Career Engine encompasses everything that takes you to get hired for the job that suits your aptitude and needs.
          
          To help you achieve career readiness, we not only aim at assisting you to prepare yourselves to face a job interview but also enable you to find the right job matching with your skillset, and guide you to succeed at your job, once you get hired.
        </div>        
      </div>
      <div class="col-md-4">
        &nbsp;
      </div>
    </div>
  </div>
</div>

<div class="container-full prasathcontent clearfix">
  &nbsp;
</div>

<?php
include_once('footer.php');
?>